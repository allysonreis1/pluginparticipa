package model;

import java.util.ArrayList;

public class ComissaoConstituicaoJustica {
	private ArrayList<ProjetoLei> listaProjetosLei;
	
	public ComissaoConstituicaoJustica () {
		listaProjetosLei = new ArrayList<ProjetoLei>();
	}

	public ArrayList<ProjetoLei> getListaProjetosLei() {
		return listaProjetosLei;
	}

	public void setListaProjetosLei(ArrayList<ProjetoLei> listaProjetosLei) {
		this.listaProjetosLei = listaProjetosLei;
	}
	
	public boolean aprovaProjetoLei (ProjetoLei umProjetoLei, String avaliacao) {
		if (avaliacao.equalsIgnoreCase("Projeto Aprovado!")) {
			return true;
		} else {
			return false;
		}
	}
}
