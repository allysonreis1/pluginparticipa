package model;

import java.util.ArrayList;

public class Administrador extends Usuario{
	
	public Administrador(String nome, ArrayList<String> telefones, 
			String email) {
		super(nome, telefones, email);
	}
	
	public boolean aprovaPostagem (Reivindicacao umaReivindicacao, String avaliacao) {
		/** O administrador deve ler a postagem e avaliar, caso 
		 * a postagem não contenha palavras ofensivas, retorna true, caso contrario false.
		 */
		if (avaliacao.equalsIgnoreCase("PostagemValida!")) {
			return true;
		} else {
			return false;
		}
	}
}
